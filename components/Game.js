import React, { useState, useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import { View, TouchableOpacity, FlatList, StyleSheet, TextInput, Text } from "react-native";

import { pTurn } from "../actions/actions-types";

const Game = () => {
  // Global State
  const state = useSelector((state) => state);
  const dispatch = useDispatch();

  // Local State
  const [userInput, setUserInput] = useState(state.juniper.currentNumber);
  const [isValid, setValidity] = useState(false);

  const userInputHandler = (e) => {
    if (e === "") {
      setUserInput("");
      setValidity(false);
    }

    if (!isNaN(e)) {
      if (e <= state.juniper.nMax) {
        setUserInput(e);
        e > 0 ? setValidity(true) : null;
      }
    }
  };

  useEffect(() => {
    setValidity(false);
  }, [state.juniper.isGameOver]);

  return (
    <>
      {/* debug */}
      {/* <Text>Game started : {state.juniper.gameStarted.toString()}</Text> */}
      <View style={styles.playContainer}>
        <TextInput
          style={styles.userInput}
          placeholder={state.juniper.currentTurn == 1 ? "Entrez un nombre pour débuter la partie" : "A votre tour"}
          placeholderTextColor="#333"
          onChangeText={(e) => userInputHandler(e)}
          clearTextOnFocus
          value={`${userInput}`}
        />
        <TouchableOpacity style={styles.buttonOk} onPress={() => dispatch(pTurn(parseInt(userInput)))} disabled={!isValid}>
          <Text style={styles.buttonOkText}>OK</Text>
        </TouchableOpacity>
      </View>
      <View style={styles.listsContainer}>
        {state.juniper.playerChoices.length > 0 ? (
          <View style={styles.listContainer}>
            <Text style={styles.listHeader}>Player</Text>
            <FlatList
              style={styles.list}
              data={state.juniper.playerChoices}
              extraData={state.juniper.playerChoices.length}
              renderItem={(item) => <Text style={styles.listItem}>{item.item}</Text>}
              keyExtractor={() => `${Math.floor(Math.random() * 100000000)}`}
            ></FlatList>
          </View>
        ) : null}
        {state.juniper.cpuChoices.length > 0 ? (
          <View style={styles.listContainer}>
            <Text style={styles.listHeader}>CPU</Text>
            <FlatList
              style={styles.list}
              data={state.juniper.cpuChoices}
              extraData={state.juniper.cpuChoices.length}
              renderItem={(item) => <Text style={styles.listItem}>{item.item}</Text>}
              keyExtractor={() => `${Math.floor(Math.random() * 100000000)}`}
            ></FlatList>
          </View>
        ) : null}
      </View>
      {/* debug */}
      <Text>{state.juniper.validChoices.toString()}</Text>
    </>
  );
};

export default Game;

const styles = StyleSheet.create({
  playContainer: {
    flexDirection: "row",
    margin: 15,
  },
  userInput: {
    borderWidth: 1,
    borderStyle: "solid",
    borderColor: "#ccc",
    borderRightWidth: 0,
    borderRadius: 0,
    padding: 10,
    width: "75%",
  },
  buttonOk: {
    backgroundColor: "#ccc",
    width: "25%",
    padding: 10,
  },
  buttonOkText: {
    color: "white",
    fontSize: 24,
    textAlign: "center",
  },
  listsContainer: {
    margin: 20,
    flexDirection: "row",
    justifyContent: "center",
  },
  listContainer: {
    margin: 10,
    width: "30%",
  },
  list: {
    backgroundColor: "#ddd",
  },
  listHeader: {
    fontSize: 18,
    fontWeight: "bold",
    backgroundColor: "#aaa",
    borderWidth: 1,
    borderStyle: "solid",
    borderColor: "#666",
    textAlign: "center",
    padding: 5,
  },
  listItem: {
    borderWidth: 1,
    borderStyle: "solid",
    borderColor: "#666",
    borderTopWidth: 0,
    width: "100%",
    textAlign: "center",
    padding: 5,
  },
});
