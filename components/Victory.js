import React, { useEffect } from "react";
import { View, Text, StyleSheet, TouchableOpacity } from "react-native";
import { useSelector, useDispatch } from "react-redux";

import { refreshScores, init } from "../actions/actions-types";

const Victory = ({ navigation }) => {
  const state = useSelector((state) => state);
  const dispatch = useDispatch();
  useEffect(() => {
    console.log(state.juniper);
    dispatch(refreshScores({ nbTurns: state.juniper.currentTurn, winner: state.juniper.winner }));
    // Can do : Ajouter le détail de chaque match ( pour pouvoir réafficher le détail de la partie au clic sur chaque score ) ?
  }, []);

  return (
    <View style={styles.victoryContainer}>
      <Text style={styles.victoryText}>{`Victoire de ${state.juniper.winner}`}</Text>
      <TouchableOpacity
        style={styles.button}
        onPress={() => {
          navigation.navigate("Home");
          dispatch(init());
        }}
      >
        <Text>Retour Home</Text>
      </TouchableOpacity>
      <TouchableOpacity
        style={styles.button}
        onPress={() => {
          dispatch(init());
        }}
      >
        <Text>Replay</Text>
      </TouchableOpacity>
    </View>
  );
};

export default Victory;

const styles = StyleSheet.create({
  victoryContainer: {
    justifyContent: "center",
    alignItems: "center",
    height: "100%",
  },
  victoryText: { alignSelf: "center", textAlign: "center", fontSize: 27, fontWeight: "bold", width: "100%" },
});
