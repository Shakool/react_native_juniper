import React from "react";
import { useDispatch } from "react-redux";
import { View, Text, TouchableOpacity, StyleSheet } from "react-native";

import { startGame } from "../actions/actions-types";

const PreGame = () => {
  const dispatch = useDispatch();

  return (
    <View>
      <Text>Bienvenue sur Juniper Green</Text>
      <TouchableOpacity onPress={() => dispatch(startGame())}>
        <Text style={styles.preStartButton}>Play</Text>
      </TouchableOpacity>
    </View>
  );
};

export default PreGame;

const styles = StyleSheet.create({
  preStartContainer: {},
  preStartText: {},
  preStartButton: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "#DDDDDD",
    margin: 50,
    padding: 30,
  },
});
