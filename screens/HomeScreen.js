import React, { useEffect, useContext } from "react";
import { Text, Alert, StyleSheet } from "react-native";
import { TouchableOpacity } from "react-native-gesture-handler";

import { useSelector, useDispatch } from "react-redux";

const HomeScreen = ({ navigation }) => {
  // Hook qui permet d'acceder à un store du combine reducer facilement à l'aide de sa clé.
  // Hook version : //   const [state, dispatch] = useContext(Context);
  const state = useSelector((state) => state);
  const dispatch = useDispatch();

  //console.log(state);

  useEffect(() => {
    dispatch({ type: "INIT" });
  }, []);

  return (
    <>
      <TouchableOpacity style={styles.button} onPress={() => navigation.navigate("Game")}>
        <Text>Game</Text>
      </TouchableOpacity>
      <TouchableOpacity style={styles.button} onPress={() => navigation.navigate("Score")}>
        <Text>Score</Text>
      </TouchableOpacity>
    </>
  );
};

export default HomeScreen;

const styles = StyleSheet.create({
  button: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "#DDDDDD",
    margin: 50,
    padding: 30,
  },
});
