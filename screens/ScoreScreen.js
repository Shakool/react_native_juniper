import React, { useEffect, useContext } from "react";
import { Text, Alert, StyleSheet, FlatList } from "react-native";
import { useSelector } from "react-redux";

// import { Context } from "../../reducer/ContextProvider";

const Score = ({ navigation }) => {
  //   const [state, dispatch] = useContext(Context);
  const state = useSelector((state) => state);

  //   useEffect(() => {
  //     dispatch({ type: "INIT" });
  //   }, []);
  console.log(state);
  return (
    <>
      {state.score.gameHistory.length > 0 ? (
        <FlatList
          style={styles.list}
          data={state.score.gameHistory}
          renderItem={(item) => {
            console.log(item);
            return <Text>{`${item.item.id}. | Gagnant: ${item.item.winner} | En ${item.item.nbTurns} tours`}</Text>;
          }}
          keyExtractor={() => `${Math.floor(Math.random() * 100000000)}`}
        ></FlatList>
      ) : (
        <Text>Pas de scores à afficher</Text>
      )}
    </>
  );
};

export default Score;

const styles = StyleSheet.create({
  button: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "#DDDDDD",
    margin: 50,
    padding: 30,
  },
});
