import React, { useEffect } from "react";
import { Alert } from "react-native";

// import { Context } from "../../reducer/ContextProvider";
import { useSelector } from "react-redux";

import Game from "../components/Game";
import PreGame from "../components/PreGame";
import Victory from "../components/Victory";

const GameScreen = ({ navigation }) => {
  //   const [state, dispatch] = useContext(Context);
  // Global State
  const state = useSelector((state) => state);

  useEffect(() => {
    state.juniper.alert != "" ? Alert.alert(state.juniper.alert) : null;
  }, [state.juniper.alert]);

  const gameState = state.juniper.isGameOver ? <Victory navigation={navigation} /> : state.juniper.gameStarted ? <Game /> : <PreGame />;
  return gameState;
};

export default GameScreen;
