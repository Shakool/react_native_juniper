import { INIT, INIT_SCORES, PLAYER_TURN, COMPUTER_TURN, GAMESTART, REFRESH_SCORES } from "../constants/actions";

export const init = () => {
  return { type: INIT };
};

export const initScores = () => {
  return { type: INIT_SCORES };
};

export const pTurn = (payload) => {
  return { type: PLAYER_TURN, payload };
};

export const cTurn = (payload) => {
  return { type: COMPUTER_TURN, payload };
};

export const startGame = () => {
  return { type: GAMESTART };
};

export const refreshScores = (payload) => {
  return { type: REFRESH_SCORES, payload };
};

// export const action = (payload) => {
//   return { type: ACTION, payload };
// };
