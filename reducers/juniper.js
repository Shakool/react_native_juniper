import * as _ from "lodash";

import { INIT, GAMESTART, PLAYER_TURN } from "../constants/actions";

const initialState = {
  currentTurn: 0,
  currentNumber: "",
  currentPlayer: "player",
  validChoices: [],
  forbiddenNumbers: [],
  playerChoices: [],
  cpuChoices: [],
  nMax: 5,
  gameStarted: false,
  isGameOver: false,
  winner: "",
  alert: "",
};

const store = _.cloneDeep(initialState);

export default (state = store, action = {}) => {
  let newState = _.cloneDeep(state);
  switch (action.type) {
    case INIT:
      console.log(initialState);
      return _.cloneDeep(initialState);
    case GAMESTART:
      newState.gameStarted = true;
      newState.currentTurn = 1;
      return { ...newState };
    case PLAYER_TURN:
      // Player Turn :
      console.log(newState);
      if (!newState.forbiddenNumbers.includes(action.payload)) {
        newState.alert = "";
        newState.currentTurn = newState.currentTurn + 1;

        newState.currentPlayer = "player";
        newState.forbiddenNumbers.push(action.payload);
        newState.playerChoices.push(action.payload);
        newState.validChoices = getValidChoices(action.payload, newState.nMax, newState.forbiddenNumbers);

        // Victory Check :
        if (newState.validChoices.length == 0) {
          newState.isGameOver = true;
          newState.winner = newState.currentPlayer;
          return { ...newState };
        }

        //CPU Turn :
        let cpuChoice = newState.validChoices[Math.floor(Math.random() * newState.validChoices.length)];
        newState.currentPlayer = "cpu";
        newState.forbiddenNumbers.push(parseInt(cpuChoice));
        newState.currentNumber = cpuChoice;
        newState.cpuChoices.push(cpuChoice);
        newState.validChoices = getValidChoices(cpuChoice, newState.nMax, newState.forbiddenNumbers);

        // Victory Check :
        if (newState.validChoices.length == 0) {
          newState.isGameOver = true;
          newState.winner = newState.currentPlayer;
          return { ...newState };
        }
      } else {
        newState.alert = "Ce nombre à déjà été utilisé";
      }
      return { ...newState };
    //   case ACTION:
    //     return {...newState};
    default:
      return state;
  }
};

const getValidChoices = (n, nMax, forbidden) => {
  let possibleResponses = [...Array(nMax).keys()].map((num) => num + 1);

  possibleResponses = possibleResponses.map((val) => {
    return n % val == 0 || val * n <= nMax ? val : null;
  });

  possibleResponses = possibleResponses.filter((val) => {
    return !forbidden.includes(val) && val != null;
  });

  return possibleResponses;
};
