import * as _ from "lodash";

import { REFRESH_SCORES, INIT_SCORES } from "../constants/actions";

const initialState = {
  gameHistory: [],
};

export default (state = initialState, action = {}) => {
  let newState = _.cloneDeep(state);
  console.log(newState);
  switch (action.type) {
    case INIT_SCORES:
      return _.cloneDeep(initialState);
    case REFRESH_SCORES:
      let game = {
        id: newState.gameHistory.length + 1,
        nbTurns: action.payload.nbTurns,
        winner: action.payload.winner,
      };
      newState.gameHistory.push(game);
      return { ...newState };
    default:
      return state;
  }
};
