import React from "react";
import { StatusBar } from "react-native";
import { SafeAreaProvider } from "react-native-safe-area-context";

import { createStore } from "redux";
import { Provider } from "react-redux";

import Nav from "./Nav";
import reducer from "./reducers/index";

const store = createStore(reducer);

export default function App() {
  return (
    <SafeAreaProvider>
      <Provider store={store}>
        <Nav />
      </Provider>
      <StatusBar style="auto" />
    </SafeAreaProvider>
  );
}
